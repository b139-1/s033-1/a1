//JSON Web Token

const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";


/*
JWT Method
.sign(<data>, <secret_key>, {<options>})
.verify(<token>, <secret_key>, <callback_fn>)
.decode()
*/

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization
	token = token.slice(7, token.length)
	if(typeof token !== "undefined") {
		return jwt.verify(token, secret, (err, result) => {
			if(err) {
				return res.send({auth: "failed"}); 
			} else {
				return next();
			}
		})
	}
}

module.exports.decode = (token) => {
	token = token.slice(7, token.length)
	if(typeof token != "undefined") {
		return jwt.verify(token, secret, (err, result) => {
			if(err) {
				return null;
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
}