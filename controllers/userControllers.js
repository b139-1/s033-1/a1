const auth = require("./../auth.js")
const User = require("./../models/User.js");
const bcrypt = require("bcrypt");


module.exports.checkEmail = (data) => {
	const {email} = data

	return User.findOne({email: email}).then((result, err) => {
		if(result != null) {
			return `Email already exists!`;
		} else {
			if(err) {
				return false;
			} else {
				return true;
			}
		}
	})
}

module.exports.register = (data) => {
	//console.log(data)
	let newUser = new User({
		firstname: data.firstname,
		lastname: data.lastname,
		email: data.email,
		password: bcrypt.hashSync(data.password, 10),
		mobileNo: data.mobileNo
	});

	return newUser.save().then((result, err) => {
		if(err) {
			return false;
		} else {
			return true;
		}
	});
}

module.exports.getAllUsers = () => {
	return User.find().then((result, error) => {
		if(result) {
			return result;
		} else {
			return false;
		}
	});
}

module.exports.login = (data) => {
	const {email, password} = data;
	return User.findOne({email: email}).then((result, err) => {
		if(result == null) {
			return false;
		} else {
			let isSamePassword = bcrypt.compareSync(password, result.password);
			if(isSamePassword == true) {
				return {access: auth.createAccessToken(result)};
			} else {
				return false;
			}
		}
	});
}

module.exports.getProfile = (data) => {
	const {id} = data
	return User.findById(id).then((result, err) => {
		if(result == null){
			return false;
		} else {
			result.password = ""
			return result
		}
	})
}